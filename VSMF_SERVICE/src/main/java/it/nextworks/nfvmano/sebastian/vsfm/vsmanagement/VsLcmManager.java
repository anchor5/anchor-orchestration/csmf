/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.sebastian.vsfm.vsmanagement;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.catalogue.blueprint.BlueprintCatalogueUtilities;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsBlueprint;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsDescriptor;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsdNstTranslationRule;
import it.nextworks.nfvmano.catalogue.blueprint.messages.QueryVsBlueprintResponse;
import it.nextworks.nfvmano.catalogue.blueprint.repo.TranslationRuleRepository;
import it.nextworks.nfvmano.catalogue.blueprint.services.VsBlueprintCatalogueService;
import it.nextworks.nfvmano.catalogue.blueprint.services.VsDescriptorCatalogueService;
import it.nextworks.nfvmano.catalogue.translator.NsInstantiationInfo;
import it.nextworks.nfvmano.catalogue.translator.TranslatorService;
import it.nextworks.nfvmano.libs.vs.common.exceptions.*;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstanceStatus;
import it.nextworks.nfvmano.libs.vs.common.nsmf.interfaces.NsmfLcmProvisioningInterface;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.InstantiateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.TerminateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.sebastian.record.VsRecordService;
import it.nextworks.nfvmano.sebastian.record.elements.NetworkSliceStatus;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceInstance;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceStatus;
import it.nextworks.nfvmano.sebastian.vsfm.VsLcmService;
import it.nextworks.nfvmano.sebastian.vsfm.VsmfUtils;
import it.nextworks.nfvmano.sebastian.vsfm.engine.messages.*;
import it.nextworks.nfvmano.sebastian.vsfm.interfaces.VsLcmConsumerInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Entity in charge of managing the lifecycle
 * of a single vertical service instance
 *
 * @author nextworks
 */
public class VsLcmManager {

    private static final Logger log = LoggerFactory.getLogger(VsLcmManager.class);
    ObjectMapper mapper = new ObjectMapper();
    private String vsiId;
    private String vsiName;
    private VsRecordService vsRecordService;
    private TranslatorService translatorService;
    private VsDescriptorCatalogueService vsDescriptorCatalogueService;
    //Used to retrieve the vsb atomic component placement
    private VsBlueprintCatalogueService vsBlueprintCatalogueService;
    private VsLcmService vsLcmService;
    private VerticalServiceStatus internalStatus;
    private NsmfLcmProvisioningInterface nsmfLcmProvider;
    private VsmfUtils vsmfUtils;
    private String networkSliceId;
    //Key: VSD ID; Value: VSD
    private VsBlueprint vsBlueprint;
    private Map<String, VsDescriptor> vsDescriptors = new HashMap<>();
    private String tenantId;
    //Key: nssiIdM Value: NfvNsInstantiationInfo
    private Map<String, NsInstantiationInfo> nssNfvInstantiationInfos = new HashMap<>();
    private NsInstantiationInfo storedNsInstantiationInfo;
    private InstantiateVsiRequestMessage storedInstantiateVsiRequestMessage;
    //Registers the list of network slice ids which are still pending to be modified due to
    //an arbitrator impacted NetworkSliceInstance
    private List<String> pendingNsiModificationIds = new ArrayList<>();
    private TranslationRuleRepository translationRuleRepository;

    /**
     * Constructor
     *
     * @param vsiId                        ID of the vertical service instance
     * @param vsRecordService              wrapper of VSI record
     * @param vsDescriptorCatalogueService repo of VSDs
     * @param translatorService            translator service
     * @param vsLcmService                 engine
     * @param nsmfLcmProvider
     * @param vsmfUtils
     */
    public VsLcmManager(String vsiId,
                        String vsiName,
                        VsRecordService vsRecordService,
                        VsDescriptorCatalogueService vsDescriptorCatalogueService,
                        VsBlueprintCatalogueService vsBlueprintCatalogueService,
                        TranslatorService translatorService,
                        VsLcmService vsLcmService,
                        NsmfLcmProvisioningInterface nsmfLcmProvider,
                        VsmfUtils vsmfUtils,
                        TranslationRuleRepository translationRuleRepository
    ) {
        this.vsiId = vsiId;
        this.vsiName = vsiName;
        this.vsRecordService = vsRecordService;
        this.vsDescriptorCatalogueService = vsDescriptorCatalogueService;
        this.translatorService = translatorService;
        this.internalStatus = VerticalServiceStatus.INSTANTIATING;
        this.vsLcmService = vsLcmService;
        this.nsmfLcmProvider = nsmfLcmProvider;
        this.vsmfUtils = vsmfUtils;
        this.vsBlueprintCatalogueService = vsBlueprintCatalogueService;
        this.translationRuleRepository = translationRuleRepository;
    }

    public void receiveMessage(String message) {
        log.debug("Received message for VSI " + vsiId + "\n" + message);

        try {
            ObjectMapper mapper = new ObjectMapper();
            VsmfEngineMessage em = mapper.readValue(message, VsmfEngineMessage.class);
            VsmfEngineMessageType type = em.getType();

            switch (type) {
                case INSTANTIATE_VSI_REQUEST: {
                    log.debug("Processing VSI instantiation request.");
                    InstantiateVsiRequestMessage instantiateVsRequestMsg = (InstantiateVsiRequestMessage) em;
                    processInstantiateRequest(instantiateVsRequestMsg);
                    break;
                }
                case TERMINATE_VSI_REQUEST: {
                    log.debug("Processing VSI termination request.");
                    TerminateVsiRequestMessage terminateVsRequestMsg = (TerminateVsiRequestMessage) em;
                    processTerminateRequest(terminateVsRequestMsg);
                    break;
                }
                case NOTIFY_NSI_STATUS_CHANGE: {
                    log.debug("Processing NSI status change notification.");
                    NotifyNsiStatusChange notifyNsiStatusChangeMsg = (NotifyNsiStatusChange) em;
                    processNsiStatusChangeNotification(notifyNsiStatusChangeMsg);
                    break;
                }
                default:
                    log.error("Received message with not supported type. Skipping.");
                    break;
            }
        } catch (JsonParseException e) {
            manageVsError("Error while parsing message: " + e.getMessage());
        } catch (JsonMappingException e) {
            manageVsError("Error in Json mapping: " + e.getMessage());
        } catch (IOException e) {
            manageVsError("IO error when receiving json message: " + e.getMessage());
        } catch (Exception e) {
            log.error("Unhandled exception");
            manageVsError("Unhandled error: " + e.getMessage());
        }
    }

    void processInstantiateRequest(InstantiateVsiRequestMessage msg) {
        if (internalStatus != VerticalServiceStatus.INSTANTIATING) {
            manageVsError("Received instantiation request in wrong status. Skipping message");
            return;
        }
        this.storedInstantiateVsiRequestMessage = msg;
        String vsdId = msg.getRequest().getVsdId();

        log.debug("Instantiating Vertical Service " + vsiId + " with VSD " + vsdId);
        log.debug("Vertical Service Instance with id {} is {}", vsiId, internalStatus);

        try {
            VsDescriptor vsd = vsDescriptorCatalogueService.getVsd(vsdId);
            this.vsDescriptors.put(vsdId, vsd);
            this.tenantId = msg.getRequest().getTenantId();
            QueryVsBlueprintResponse vsBlueprintResponse = vsBlueprintCatalogueService.queryVsBlueprint(new GeneralizedQueryRequest(BlueprintCatalogueUtilities.buildVsBlueprintFilter(vsd.getVsBlueprintId(), tenantId), null));
            this.vsBlueprint = vsBlueprintResponse.getVsBlueprintInfo().get(0).getVsBlueprint();
            String vsbName = this.vsBlueprint.getName();
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_INSTANTIATION_START, vsbName));
            List<String> vsdIds = new ArrayList<>();
            vsdIds.add(vsdId);
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_TRANSLATION_START, vsbName));
            Map<String, NsInstantiationInfo> nsInfo = translatorService.translateVsd(vsdIds);
            storedNsInstantiationInfo = nsInfo.get(vsdId);
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_TRANSLATION_END));
            log.debug("The VSD has been translated in the required network slice characteristics");
            log.debug("Translator response: " + nsInfo);

            internalProcessInstantiationRequest();
        } catch (Exception e) {
            manageVsError("Error while instantiating VS " + vsiId + ": " + e.getMessage(), e);
        }
    }

    private void internalProcessInstantiationRequest() throws FailedOperationException, MalformattedElementException,
            NotPermittedOperationException, NotExistingEntityException, MethodNotImplementedException {
        VsDescriptor vsd = this.vsDescriptors.get(this.storedInstantiateVsiRequestMessage.getRequest().getVsdId());
        log.debug("A new network slice should be instantiated for the Vertical Service instance " + vsiId);
        try {
            log.debug("NSInstantiationInfo for VSI " + vsiId + ": " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(storedNsInstantiationInfo));
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize NFV NS instantiation info:", e);
        }
        Map<String, String> vsdParameters = vsd.getQosParameters();
        List<VsdNstTranslationRule> rules = translationRuleRepository.findByBlueprintId(vsd.getVsBlueprintId());
        VsdNstTranslationRule targetRule = null;
        for (VsdNstTranslationRule rule : rules) {
            if (rule.matchesVsdParameters(vsdParameters))
                targetRule = rule;
        }
        if (targetRule == null) {
            log.debug("Impossible to find a translation rule matching the given descriptor parameters");
            manageVsError("Impossible to find a translation rule matching the given descriptor parameters");
            return;
        }

        CreateNsiIdRequest request;
        log.debug("Using VSMF single NSMF domain functionality");
        request = new CreateNsiIdRequest(storedNsInstantiationInfo.getNstId(),
                "NS - " + vsiName,
                "Network slice for VS " + vsiName,
                vsiId);
        UUID nsiId = nsmfLcmProvider.createNetworkSliceIdentifierFromNst(request, tenantId);

        log.debug("Network Slice ID " + nsiId + " created for VSI " + vsiId);
        networkSliceId = String.valueOf(nsiId);
        vsRecordService.setNsiInVsi(vsiId, networkSliceId);
        log.debug("Record updated with info about NSI and VSI association");
        InstantiateNsiRequest instantiateNsiReq = new InstantiateNsiRequest(nsiId);
        log.debug("Sending request to instantiate network slice");
        log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_INSTANTIATION_START, vsBlueprint.getName(), String.valueOf(nsiId)));
        nsmfLcmProvider.instantiateNetworkSlice(instantiateNsiReq, tenantId);
    }

    void processTerminateRequest(TerminateVsiRequestMessage msg) {
        if (!msg.getVsiId().equals(vsiId)) {
            throw new IllegalArgumentException(String.format("Wrong VSI ID: %s", msg.getVsiId()));
        }

        log.debug("Processing terminate request for VSI {}, with status {}", msg.getVsiId(), internalStatus);
        if (internalStatus != VerticalServiceStatus.INSTANTIATED && internalStatus != VerticalServiceStatus.MODIFIED) {
            manageVsError("Received termination request in wrong status. Skipping message.");
            return;
        }

        log.debug("Terminating Vertical Service " + vsiId);
        log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_TERMINATION_START));
        this.internalStatus = VerticalServiceStatus.TERMINATING;
        try {
            vsRecordService.setVsStatus(vsiId, VerticalServiceStatus.TERMINATING);
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_TERMINATION_START, vsBlueprint.getName(), networkSliceId));
            //log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_TERMINATION_START));
            log.debug("Network slice " + networkSliceId + " must be terminated");
            vsRecordService.setNsiStatusInVsi(vsiId, networkSliceId, NetworkSliceStatus.TERMINATING);
            TerminateNsiRequest terminateNsiRequest = new TerminateNsiRequest();
            terminateNsiRequest.setNsiId(UUID.fromString(networkSliceId));
            nsmfLcmProvider.terminateNetworkSliceInstance(terminateNsiRequest, null);
        } catch (Exception e) {
            manageVsError("Error while terminating VS " + vsiId + ": " + e.getMessage());
        }
    }

    void processNsiStatusChangeNotification(NotifyNsiStatusChange msg) {
        if (!((internalStatus == VerticalServiceStatus.INSTANTIATING) || (internalStatus == VerticalServiceStatus.TERMINATING) || (internalStatus == VerticalServiceStatus.UNDER_MODIFICATION))) {
            manageVsError("Received NSI status change notification in wrong status. Skipping message");
            return;
        }
        NetworkSliceInstanceStatus nsStatusChange = msg.getStatusChange();
        String nsiId = msg.getNsiId();
        try {
            switch (nsStatusChange) {
                // TODO: check if CREATED or INSTANTIATED
                case CREATED:
                case INSTANTIATED: {
                    nsStatusChangeOperations(NetworkSliceStatus.INSTANTIATED, nsiId);
                    break;
                }
                case TERMINATED: {
                    nsStatusChangeOperations(NetworkSliceStatus.TERMINATED, nsiId);
                    break;
                }
                case FAILED: {
                    manageVsError("Received notification about network slice " + msg.getNsiId() + " failure");
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {
            manageVsError("Error while processing NSI status change notification: " + e.getMessage());
        }
    }

    private void nsStatusChangeOperations(NetworkSliceStatus status, String nsiId) throws NotExistingEntityException, Exception {
        VerticalServiceInstance vsi = vsRecordService.getVsInstance(vsiId);
        if (status == NetworkSliceStatus.INSTANTIATED && internalStatus == VerticalServiceStatus.INSTANTIATING) {
            log.debug("Updating Vertical service instance internal network slice subnet");
            vsRecordService.setNsiStatusInVsi(vsiId, nsiId, NetworkSliceStatus.INSTANTIATED);
            internalStatus = VerticalServiceStatus.INSTANTIATED;
            //log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_INSTANTIATION_END));
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_INSTANTIATION_END, vsBlueprint.getName(), String.valueOf(nsiId)));
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_INSTANTIATION_END));
        } else if (status == NetworkSliceStatus.TERMINATED && internalStatus == VerticalServiceStatus.TERMINATING) {
            vsRecordService.setNsiStatusInVsi(vsiId, nsiId, NetworkSliceStatus.TERMINATED);
            internalStatus = VerticalServiceStatus.TERMINATED;
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_TERMINATION_END, vsBlueprint.getName(), String.valueOf(nsiId)));
            //log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_NSI_TERMINATION_END));
            log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_TERMINATION_END, vsBlueprint.getName(), String.valueOf(nsiId)));
            //log.debug(MetricsLogger.getLogMessage(vsiId, MetricsLogger.VSI_TERMINATION_END));
        } else {
            manageVsError("Received notification about NSI creation in wrong status");
            return;
        }
        vsRecordService.setVsStatus(vsiId, internalStatus);
        log.debug("Vertical Service Instance with id {} is {}", vsiId, internalStatus);
        //if (internalStatus == VerticalServiceStatus.TERMINATED) vsLcmService.removeVerticalServiceLcmManager(vsiId);
    }

    private void manageVsError(String errorMessage, Exception e) {
        log.error(errorMessage, e);
        vsRecordService.setVsFailureInfo(vsiId, errorMessage);
    }

    private void manageVsError(String errorMessage) {
        log.error(errorMessage);
        vsRecordService.setVsFailureInfo(vsiId, errorMessage);
    }
}