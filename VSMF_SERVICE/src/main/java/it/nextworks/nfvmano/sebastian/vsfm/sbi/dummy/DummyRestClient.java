package it.nextworks.nfvmano.sebastian.vsfm.sbi.dummy;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.libs.vs.common.exceptions.*;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstanceStatus;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceSubnetInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdFromNestRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.InstantiateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.TerminateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.vs.common.vsmf.elements.NsiNotifType;
import it.nextworks.nfvmano.libs.vs.common.vsmf.message.VsmfNotificationMessage;
import it.nextworks.nfvmano.sebastian.vsfm.VsLcmService;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.AbstractNsmfDriver;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.NsmfType;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.dummy.elements.DummyTranslationInformation;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.dummy.repos.DummyTranslationInformationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DummyRestClient extends AbstractNsmfDriver {

    private static final Logger log = LoggerFactory.getLogger(DummyRestClient.class);

    private ObjectMapper mapper;

    private VsLcmService vsLcmService;
    private DummyTranslationInformationRepository dummyTranslationInformationRepository;

    public DummyRestClient(VsLcmService vsLcmService, DummyTranslationInformationRepository dummyTranslationInformationRepository) {
        super(NsmfType.DUMMY);
        this.mapper = new ObjectMapper();
        this.vsLcmService = vsLcmService;
        this.dummyTranslationInformationRepository = dummyTranslationInformationRepository;
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNst(CreateNsiIdRequest request, String tenantId) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Processing request to create a new network slicer identifier");
        request.isValid();

        UUID nsiId = UUID.randomUUID();

        DummyTranslationInformation dummyTranslationInformation = new DummyTranslationInformation(nsiId.toString(), request.getNstId(), null);
        Long id = dummyTranslationInformationRepository.saveAndFlush(dummyTranslationInformation).getId();

        return nsiId;
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNest(CreateNsiIdFromNestRequest createNsiIdFromNestRequest, String s) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Method not implemented");
        return null;
    }

    @Override
    public void instantiateNetworkSlice(InstantiateNsiRequest request, String tenantId) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Processing request to instantiate a new network slice");
        request.isValid();

        VsmfNotificationMessage notification = new VsmfNotificationMessage(request.getNsiId(), NsiNotifType.STATUS_CHANGED, NetworkSliceInstanceStatus.CREATED);
        vsLcmService.notifyNetworkSliceStatusChange(notification);
    }

    @Override
    public void terminateNetworkSliceInstance(TerminateNsiRequest request, String tenantId) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Processing request to terminate a new network slice");
        request.isValid();

        Optional<DummyTranslationInformation> optional = dummyTranslationInformationRepository.findByNsiId(request.getNsiId().toString());

        if (optional.isPresent()) {
            DummyTranslationInformation dummyTranslationInformation = optional.get();
            dummyTranslationInformationRepository.delete(dummyTranslationInformation);
        } else {
            throw new FailedOperationException("NSI with nsiId " + request.getNsiId() + " not present in DB");
        }

        VsmfNotificationMessage notification = new VsmfNotificationMessage(request.getNsiId(), NsiNotifType.STATUS_CHANGED, NetworkSliceInstanceStatus.TERMINATED);
        vsLcmService.notifyNetworkSliceStatusChange(notification);
    }

    @Override
    public List<NetworkSliceInstance> queryNetworkSliceInstance(GeneralizedQueryRequest request, String tenantId)
            throws MalformattedElementException {
        log.debug("Processing request to query a new network slice");
        request.isValid();

        String nsiId = null;
        List<NetworkSliceInstance> networkSliceInstances = new ArrayList<>();

        if (request.getFilter().getParameters().containsKey("NSI_ID"))
            nsiId = request.getFilter().getParameters().get("NSI_ID");
        if (nsiId != null) {
            Optional<DummyTranslationInformation> optional = dummyTranslationInformationRepository.findByNsiId(nsiId);
            if (optional.isPresent()) {
                DummyTranslationInformation dummyTranslationInformation = optional.get();
                NetworkSliceInstance networkSliceInstance = new NetworkSliceInstance(
                        UUID.fromString(dummyTranslationInformation.getNsiId()),
                        null,
                        null,
                        NetworkSliceInstanceStatus.INSTANTIATED,
                        "Dummy"
                );
                networkSliceInstances.add(networkSliceInstance);
            }
        }
        return networkSliceInstances;
    }

    @Override
    public List<NetworkSliceSubnetInstance> queryNetworkSliceSubnetInstance(GeneralizedQueryRequest generalizedQueryRequest, String s) throws MalformattedElementException {
        return null;
    }
}
