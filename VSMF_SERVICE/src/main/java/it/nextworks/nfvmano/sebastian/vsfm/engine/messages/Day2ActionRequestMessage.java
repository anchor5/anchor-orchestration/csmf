package it.nextworks.nfvmano.sebastian.vsfm.engine.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.nextworks.nfvmano.sebastian.vsfm.messages.Day2ActionRequest;

public class Day2ActionRequestMessage extends VsmfEngineMessage {

    @JsonProperty("vsiId")
    private String vsiId;

    @JsonProperty("request")
    private Day2ActionRequest request;

    /**
     * Constructor
     *
     * @param vsiId   ID of the VS to be instantiated
     * @param request VSI day2 action request
     */
    @JsonCreator
    public Day2ActionRequestMessage(@JsonProperty("vsiId") String vsiId,
                                    @JsonProperty("request") Day2ActionRequest request) {
        this.type = VsmfEngineMessageType.DAY_2_ACTION_REQUEST;
        this.vsiId = vsiId;
        this.request = request;
    }

    /**
     * @return the vsiId
     */
    public String getVsiId() {
        return vsiId;
    }

    /**
     * @return the request
     */
    public Day2ActionRequest getRequest() {
        return request;
    }

}
