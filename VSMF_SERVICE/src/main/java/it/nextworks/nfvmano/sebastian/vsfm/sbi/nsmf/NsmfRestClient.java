/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.sebastian.vsfm.sbi.nsmf;

import it.nextworks.nfvmano.libs.vs.common.exceptions.*;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceSubnetInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdFromNestRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.InstantiateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.TerminateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.AbstractNsmfDriver;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.NsmfType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST client to interact with a REST based NSMF exposing 3GPP inspired interface.
 *
 * @author nextworks
 */
public class NsmfRestClient extends AbstractNsmfDriver {

    private static final Logger log = LoggerFactory.getLogger(NsmfRestClient.class);
    private RestTemplate restTemplate;
    private String nsmfUrl;

    private static final String uuidRegex = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";

    public NsmfRestClient(String baseUrl) {
        super(NsmfType.NSMF_3GPP_LIKE);
        this.nsmfUrl = baseUrl + "/nsmf/ns/nslcm";
        this.restTemplate = new RestTemplate();
    }

    private ResponseEntity<String> performHTTPRequest(Object request, String url, HttpMethod httpMethod) {
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json");

        HttpEntity<?> httpEntity = new HttpEntity<>(request, header);

        try {
            log.info("URL performing the request to: " + url);
            ResponseEntity<String> httpResponse =
                    restTemplate.exchange(url, httpMethod, httpEntity, String.class);
            HttpStatus code = httpResponse.getStatusCode();
            log.info("Response code: " + httpResponse.getStatusCode().toString());
            return httpResponse;
        } catch (RestClientException e) {
            log.info("Message received: " + e.getMessage());
            return null;
        }
    }

    private String manageHTTPResponse(ResponseEntity<?> httpResponse, String errMsg, String okCodeMsg, HttpStatus httpStatusExpected) {
        if (httpResponse == null) {
            log.info(errMsg);
            return null;
        }

        if (httpResponse.getStatusCode().equals(httpStatusExpected)) log.info(okCodeMsg);
        else log.info(errMsg);

        log.info("Response code: " + httpResponse.getStatusCode().toString());

        if (httpResponse.getBody() == null) return null;

        log.info(("Body response: " + httpResponse.getBody().toString()));
        return httpResponse.getBody().toString();
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNst(CreateNsiIdRequest request, String tenantId)
            throws MethodNotImplementedException, FailedOperationException,
            MalformattedElementException, NotPermittedOperationException {
        String url = nsmfUrl + "/ns/nst";
        ResponseEntity<String> httpResponse = performHTTPRequest(request, url, HttpMethod.POST);
        String uuid = manageHTTPResponse(httpResponse, "Error while creating network slice identifier", "Network slice identifier correctly created", HttpStatus.CREATED);
        Pattern pattern = Pattern.compile(uuidRegex);
        Matcher matcher = pattern.matcher(uuid);
        if (!matcher.find())
            return null;
        return UUID.fromString(matcher.group(0));
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNest(CreateNsiIdFromNestRequest createNsiIdFromNestRequest, String s) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Method not implemented");
        return null;
    }

    @Override
    public void instantiateNetworkSlice(InstantiateNsiRequest request, String tenantId)
            throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException,
            MalformattedElementException, NotPermittedOperationException {
        log.info("Sending request to instantiate network Slice");
        String url = nsmfUrl + "/ns/" + request.getNsiId() + "/action/instantiate";
        ResponseEntity<String> httpResponse = performHTTPRequest(request, url, HttpMethod.PUT);
        String bodyResponse = manageHTTPResponse(httpResponse, "Error while instantiating network slice", "Network slice instantiation correctly performed", HttpStatus.ACCEPTED);
    }

    @Override
    public void terminateNetworkSliceInstance(TerminateNsiRequest request, String tenantId)
            throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException,
            MalformattedElementException, NotPermittedOperationException {
        log.info("Sending request to terminate network slice");
        String url = nsmfUrl + "/ns/" + request.getNsiId() + "/action/terminate";
        ResponseEntity<String> httpResponse = performHTTPRequest(request, url, HttpMethod.PUT);
        String bodyResponse = manageHTTPResponse(httpResponse, "Error while terminating network slice", "Network slice termination correctly performed", HttpStatus.ACCEPTED);
    }

    @Override
    public List<NetworkSliceInstance> queryNetworkSliceInstance(GeneralizedQueryRequest request, String tenantId)
            throws MalformattedElementException {
        log.info("Sending request to query network slice instance");
        String url = nsmfUrl + "/ns/";
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json");

        HttpEntity<?> httpEntity = new HttpEntity<>(request, header);

        try {
            //If the nsiID is specified a NetworkSliceInstance is sent, viceversa a List<NetworkSliceInstance> .
            if (request.getFilter() != null) {
                String nsiID = request.getFilter().getParameters().getOrDefault("NSI_ID", "");
                url += nsiID;
                ResponseEntity<NetworkSliceInstance> httpResponseAtMostOneNSI =
                        restTemplate.exchange(url, HttpMethod.GET, httpEntity, NetworkSliceInstance.class);

                List<NetworkSliceInstance> nsii = new ArrayList<NetworkSliceInstance>();
                nsii.add(httpResponseAtMostOneNSI.getBody());
                log.info("Response code: " + httpResponseAtMostOneNSI.getStatusCode().toString());
                manageHTTPResponse(httpResponseAtMostOneNSI, "Error querying network slice instance", "Query to network slice instance performed correctly", HttpStatus.OK);
                return nsii;
            } else {
                ResponseEntity<List<NetworkSliceInstance>> httpResponse = restTemplate.exchange(url, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<NetworkSliceInstance>>() {
                });
                log.info("Response code: " + httpResponse.getStatusCode().toString());
                manageHTTPResponse(httpResponse, "Error querying network slice instance", "Query to network slice instance performed correctly", HttpStatus.OK);
                return httpResponse.getBody();
            }
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<NetworkSliceSubnetInstance> queryNetworkSliceSubnetInstance(GeneralizedQueryRequest generalizedQueryRequest, String s) throws MalformattedElementException {
        log.debug("Method not implemented");
        return null;
    }
}