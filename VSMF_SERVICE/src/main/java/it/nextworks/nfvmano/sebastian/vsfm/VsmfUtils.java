package it.nextworks.nfvmano.sebastian.vsfm;

import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.interfaces.NsmfLcmProvisioningInterface;
import it.nextworks.nfvmano.libs.vs.common.query.elements.Filter;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VsmfUtils {

    private static final Logger log = LoggerFactory.getLogger(VsmfUtils.class);

    private NsmfLcmProvisioningInterface nsmfLcmProvider;

    public it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstance readNetworkSliceInstanceInformation(String nsiId, String tenantId)
            throws FailedOperationException, NotExistingEntityException {
        log.debug("Interacting with NSMF service to get information about network slice with ID " + nsiId);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NSI_ID", nsiId);
        Filter filter = new Filter(parameters);
        GeneralizedQueryRequest request = new GeneralizedQueryRequest(filter, new ArrayList<String>());
        try {
            List<NetworkSliceInstance> nsis = nsmfLcmProvider.queryNetworkSliceInstance(request, tenantId);
            if (nsis.isEmpty()) {
                log.error("Network Slice " + nsiId + " not found in NSMF service");
                throw new NotExistingEntityException("Network Slice " + nsiId + " not found in NSMF service");
            }
            return nsis.get(0);
        } catch (Exception e) {
            log.error("Error while getting network slice instance " + nsiId + ": " + e.getMessage());
            throw new FailedOperationException("Error while getting network slice instance " + nsiId + ": " + e.getMessage());
        }
    }

    public void setNsmfLcmProvider(NsmfLcmProvisioningInterface nsmfLcmProvider) {
        this.nsmfLcmProvider = nsmfLcmProvider;
    }
}
