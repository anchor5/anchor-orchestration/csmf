/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.sebastian.vsfm.sbi;

import it.nextworks.nfvmano.libs.vs.common.exceptions.*;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.elements.NetworkSliceSubnetInstance;
import it.nextworks.nfvmano.libs.vs.common.nsmf.interfaces.NsmfLcmProvisioningInterface;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdFromNestRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.CreateNsiIdRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.InstantiateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.nsmf.messages.provisioning.TerminateNsiRequest;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.sebastian.vsfm.VsLcmService;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.dummy.DummyRestClient;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.dummy.repos.DummyTranslationInformationRepository;
import it.nextworks.nfvmano.sebastian.vsfm.sbi.nsmf.NsmfRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Class used to manage the interaction with external NSMF when this is exposed via REST.
 *
 * @author nextworks
 */
@Service
public class NsmfInteractionHandler implements NsmfLcmProvisioningInterface {

    private static final Logger log = LoggerFactory.getLogger(NsmfInteractionHandler.class);

    // domainId -> Interface
    private Map<String, NsmfLcmProvisioningInterface> drivers = new HashMap<String, NsmfLcmProvisioningInterface>();

    private NsmfRestClient nsmfRestClient;

    @Autowired
    private DummyTranslationInformationRepository dummyTranslationInformationRepository;

    @Autowired
    private VsLcmService vsLcmService;

    @Value("${dummy.multidomain.enabled:false}")
    private boolean dummyMultidomainMode;

    @Value("${vsm.sbi.nsmf.defaultDomain:defaultDomain}")
    private String defaultDomain;

    @Value("${vsmf.notifications.url}")
    private String notificationCallbackUri;

    public void init() {
    }

    public void setNsmfClientConfiguration(NsmfType nsmfType, String nsmfRestServerUrl) {
        AbstractNsmfDriver nsmfRestClient;
        switch (nsmfType) {
            case NSMF_3GPP_LIKE:
                nsmfRestClient = new NsmfRestClient(nsmfRestServerUrl);
                break;
            case DUMMY:
                nsmfRestClient = new DummyRestClient(this.vsLcmService, this.dummyTranslationInformationRepository);
                break;
            default:
                throw new IllegalArgumentException("NsmfType " + nsmfType + " not supported as default domain");
        }

        this.drivers.put(defaultDomain, nsmfRestClient);
        log.info("Default rest client instantiated for {} NSP", nsmfType);
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNst(CreateNsiIdRequest request, String tenantId) throws it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException, it.nextworks.nfvmano.libs.vs.common.exceptions.MethodNotImplementedException, it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException, it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException, it.nextworks.nfvmano.libs.vs.common.exceptions.NotPermittedOperationException {

        NsmfLcmProvisioningInterface domainDriver;
        String domainId = defaultDomain;
        domainDriver = drivers.get(domainId);

        return domainDriver.createNetworkSliceIdentifierFromNst(request, tenantId);
    }

    @Override
    public void instantiateNetworkSlice(InstantiateNsiRequest request, String tenantId)
            throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException,
            MalformattedElementException, NotPermittedOperationException {

        NsmfLcmProvisioningInterface domainDriver;
        String domainId = defaultDomain;
        domainDriver = drivers.get(domainId);
        domainDriver.instantiateNetworkSlice(request, tenantId);
    }

    @Override
    public void terminateNetworkSliceInstance(TerminateNsiRequest request, String tenantId)
            throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException,
            MalformattedElementException, NotPermittedOperationException {

        NsmfLcmProvisioningInterface domainDriver;
        String domainId = defaultDomain;
        domainDriver = drivers.get(domainId);
        domainDriver.terminateNetworkSliceInstance(request, tenantId);
    }

    @Override
    public List<NetworkSliceInstance> queryNetworkSliceInstance(GeneralizedQueryRequest request, String tenantId)
            throws MalformattedElementException {

        NsmfLcmProvisioningInterface domainDriver;
        String domainId = defaultDomain;
        domainDriver = drivers.get(domainId);

        return domainDriver.queryNetworkSliceInstance(request, tenantId);
    }

    @Override
    public List<NetworkSliceSubnetInstance> queryNetworkSliceSubnetInstance(GeneralizedQueryRequest generalizedQueryRequest, String s) throws MalformattedElementException {
        return null;
    }

    @Override
    public UUID createNetworkSliceIdentifierFromNest(CreateNsiIdFromNestRequest createNsiIdFromNestRequest, String s) throws NotExistingEntityException, MethodNotImplementedException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        return null;
    }
}
