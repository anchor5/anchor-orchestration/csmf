/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.sebastian.vsfm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.nfvmano.catalogue.blueprint.BlueprintCatalogueUtilities;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsBlueprint;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsDescriptor;
import it.nextworks.nfvmano.catalogue.blueprint.messages.QueryVsBlueprintResponse;
import it.nextworks.nfvmano.catalogue.blueprint.repo.TranslationRuleRepository;
import it.nextworks.nfvmano.catalogue.blueprint.services.VsBlueprintCatalogueService;
import it.nextworks.nfvmano.catalogue.blueprint.services.VsDescriptorCatalogueService;
import it.nextworks.nfvmano.catalogue.translator.TranslatorService;
import it.nextworks.nfvmano.libs.vs.common.exceptions.*;
import it.nextworks.nfvmano.libs.vs.common.nsmf.interfaces.NsmfLcmProvisioningInterface;
import it.nextworks.nfvmano.libs.vs.common.query.elements.Filter;
import it.nextworks.nfvmano.libs.vs.common.query.messages.GeneralizedQueryRequest;
import it.nextworks.nfvmano.libs.vs.common.vsmf.message.VsmfNotificationMessage;
import it.nextworks.nfvmano.sebastian.common.ConfigurationParameters;
import it.nextworks.nfvmano.sebastian.common.Utilities;
import it.nextworks.nfvmano.sebastian.record.VsRecordService;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceInstance;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceStatus;
import it.nextworks.nfvmano.sebastian.vsfm.engine.messages.*;
import it.nextworks.nfvmano.sebastian.vsfm.interfaces.VsLcmProviderInterface;
import it.nextworks.nfvmano.sebastian.vsfm.messages.*;
import it.nextworks.nfvmano.sebastian.vsfm.vsmanagement.VsLcmManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Front-end service for managing the incoming requests
 * about creation, termination, etc. of Vertical Service instances.
 * <p>
 * It is invoked by the related REST controller and dispatches requests
 * to the centralized engine.
 *
 * @author nextworks
 */
@Service
public class VsLcmService implements VsLcmProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(VsLcmService.class);
    @Autowired
    @Qualifier(ConfigurationParameters.engineQueueExchange)
    TopicExchange messageExchange;
    @Autowired
    private VsDescriptorCatalogueService vsDescriptorCatalogueService;
    @Autowired
    private VsBlueprintCatalogueService vsBlueprintCatalogueService;
    @Autowired
    private VsRecordService vsRecordService;
    @Autowired
    private TranslatorService translatorService;
    @Value("${sebastian.admin}")
    private String adminTenant;
    private NsmfLcmProvisioningInterface nsmfLcmProvider;
    @Value("${spring.rabbitmq.host}")
    private String rabbitHost;
    @Value("${spring.rabbitmq.timout:10}")
    private int rabbitTimeout;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private VsmfUtils vsmfUtils;

    //internal map of VS LCM Managers
    //each VS LCM Manager is created when a new VSI ID is created and removed when the VSI ID is removed
    private Map<String, VsLcmManager> vsLcmManagers = new HashMap<>();

    @Autowired
    private TranslationRuleRepository translationRuleRepository;

    @Override
    public String instantiateVs(InstantiateVsRequest request) throws FailedOperationException, MalformattedElementException, NotPermittedOperationException, NotExistingEntityException {
        log.debug("Received request to instantiate a new Vertical Service instance");
        request.isValid();

        String vsdId = request.getVsdId();

        VsDescriptor vsd = vsDescriptorCatalogueService.getVsd(vsdId);

        return internalProcessInstantiateVs(vsd, request);
    }

    private String internalProcessInstantiateVs(VsDescriptor vsd, InstantiateVsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        String tenantId = request.getTenantId();
        String vsdId = request.getVsdId();
        String vsbId = vsd.getVsBlueprintId();
        VsBlueprint vsb = retrieveVsb(vsbId);
        log.debug("Retrieved VSB for the requested VSI");

        //checking configuration parameters
        Map<String, String> userData = request.getUserData();
        Set<String> providedConfigParameters = userData.keySet();
        if (!(providedConfigParameters.isEmpty())) {
            List<String> acceptableConfigParameters = vsb.getConfigurableParameters();
            for (String cp : providedConfigParameters) {
                if (!(acceptableConfigParameters.contains(cp))) {
                    log.error("The request includes a configuration parameter " + cp + " which is not present in the VSB. Not acceptable");
                    throw new MalformattedElementException("The request includes a configuration parameter " + cp + " which is not present in the VSB. Not acceptable");
                }
            }
            log.debug("Set user configuration parameters for VS instance");
        }
        log.debug("The VS instantiation request is valid");

        String vsiId = vsRecordService.createVsInstance(request.getName(), request.getDescription(), vsdId, tenantId, userData);
        initNewVsLcmManager(vsiId, request.getName());
        try {
            String topic = "lifecycle.instantiatevs." + vsiId;
            InstantiateVsiRequestMessage internalMessage = new InstantiateVsiRequestMessage(vsiId, request);
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                log.error("Error while translating internal VS instantiation message in Json format");
                vsRecordService.setVsFailureInfo(vsiId, "Error while translating internal VS instantiation message in Json format");
            }
            log.debug("Synchronous processing for VSI instantiation request completed for VSI ID " + vsiId);
            return vsiId;
        } catch (Exception e) {
            vsRecordService.setVsFailureInfo(vsiId, e.getMessage());
            throw new FailedOperationException(e.getMessage());
        }
    }

    @Override
    public QueryVsResponse queryVs(GeneralizedQueryRequest request) throws MethodNotImplementedException,
            NotExistingEntityException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug(request.toString());
        return internalQueryVs(request);
    }

    private QueryVsResponse internalQueryVs(GeneralizedQueryRequest request) throws NotPermittedOperationException, MalformattedElementException, MethodNotImplementedException, NotExistingEntityException, FailedOperationException {
        log.debug("Received a query about a Vertical Service instance");
        request.isValid();
        //At the moment the only filter accepted are:
        //1. VSI ID && TENANT ID
        //No attribute selector is supported at the moment
        Filter filter = request.getFilter();
        List<String> attributeSelector = request.getAttributeSelector();

        if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
            Map<String, String> fp = filter.getParameters();
            if (fp.size() == 2 && fp.containsKey("VSI_ID") && fp.containsKey("TENANT_ID")) {
                String vsiId = fp.get("VSI_ID");
                String tenantId = fp.get("TENANT_ID");
                log.debug("Received a query about VS instance with ID " + vsiId + " for tenant ID " + tenantId);
                VerticalServiceInstance vsi = vsRecordService.getVsInstance(vsiId);
                if (tenantId.equals(adminTenant) || tenantId.equals(vsi.getTenantId())) {
                    return new QueryVsResponse(
                            vsiId,
                            vsi.getName(),
                            vsi.getDescription(),
                            vsi.getVsdId(),
                            vsi.getStatus(),
                            vsi.getErrorMessage(),
                            null
                    );
                } else {
                    log.error("The tenant has not access to the given VSI");
                    throw new NotPermittedOperationException("Tenant " + tenantId + " has not access to VSI with ID " + vsiId);
                }
            } else {
                log.error("Received VSI query with not supported filter");
                throw new MalformattedElementException("Received VSI query with not supported filter");
            }
        } else {
            log.error("Received VSI query with attribute selector. Not supported at the moment");
            throw new MethodNotImplementedException("Received VSI query with attribute selector. Not supported at the moment");
        }
    }

    @Override
    public List<String> queryAllVsIds(GeneralizedQueryRequest request)
            throws MethodNotImplementedException, NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request about all the IDs of Vertical Service instances");
        request.isValid();

        Filter filter = request.getFilter();
        List<String> attributeSelector = request.getAttributeSelector();
        if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
            Map<String, String> fp = filter.getParameters();
            if (fp.size() == 1 && fp.containsKey("TENANT_ID")) {
                List<String> vsIds = new ArrayList<>();
                String tenantId = fp.get("TENANT_ID");
                List<VerticalServiceInstance> vsInstances = new ArrayList<>();
                if (tenantId.equals(adminTenant)) {
                    log.debug("VSI ID query for admin: returning all the VSIs");
                    vsInstances = vsRecordService.getAllVsInstances();
                } else {
                    log.debug("VSI ID query for tenant " + tenantId);
                    vsInstances = vsRecordService.getAllVsInstances(tenantId);
                }
                for (VerticalServiceInstance vsi : vsInstances) vsIds.add(vsi.getVsiId());
                return vsIds;
            } else {
                log.error("Received all VSI ID query with not supported filter");
                throw new MalformattedElementException("Received VSI query with not supported filter");
            }
        } else {
            log.error("Received VSI query with attribute selector. Not supported at the moment.");
            throw new MethodNotImplementedException("Received VSI query with attribute selector. Not supported at the moment");
        }
    }

    @Override
    public List<VerticalServiceInstance> queryAllVsInstances(GeneralizedQueryRequest request)
            throws MethodNotImplementedException, MalformattedElementException {
        log.debug("Received request about all the IDs of Vertical Service instances");
        request.isValid();
        Filter filter = request.getFilter();
        List<String> attributeSelector = request.getAttributeSelector();
        if ((attributeSelector == null) || (attributeSelector.isEmpty())) {
            Map<String, String> fp = filter.getParameters();
            if (fp.size() == 1 && fp.containsKey("TENANT_ID")) {
                List<String> vsIds = new ArrayList<>();
                String tenantId = fp.get("TENANT_ID");
                List<VerticalServiceInstance> vsInstances = new ArrayList<>();
                if (tenantId.equals(adminTenant)) {
                    log.debug("VSI ID query for admin: returning all the VSIs");
                    vsInstances = vsRecordService.getAllVsInstances();
                } else {
                    log.debug("VSI ID query for tenant " + tenantId);
                    vsInstances = vsRecordService.getAllVsInstances(tenantId);
                }
                return vsInstances;
            } else {
                log.error("Received all VSI ID query with not supported filter.");
                throw new MalformattedElementException("Received VSI query with not supported filter");
            }
        } else {
            log.error("Received VSI query with attribute selector. Not supported at the moment");
            throw new MethodNotImplementedException("Received VSI query with attribute selector. Not supported at the moment");
        }
    }

    @Override
    public void terminateVs(TerminateVsRequest request) throws MethodNotImplementedException,
            NotExistingEntityException, FailedOperationException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Received request to terminate a Vertical Service instance");
        request.isValid();

        String tenantId = request.getTenantId();
        String vsiId = request.getVsiId();

        VerticalServiceInstance vsi = vsRecordService.getVsInstance(vsiId);
        if (tenantId.equals(adminTenant) || vsi.getTenantId().equals(tenantId)) {
            log.debug("The termination request is valid.");
            log.debug("Synchronous processing for VSI termination request completed for VSI ID " + vsiId);
            terminateVs(vsiId, request);
        } else {
            log.debug("Tenant " + tenantId + " is not allowed to terminate VS instance " + vsiId);
            throw new NotPermittedOperationException("Tenant " + tenantId + " is not allowed to terminate VS instance " + vsiId);
        }
    }

    @Override
    public void purgeVs(PurgeVsRequest request)
            throws MethodNotImplementedException, NotExistingEntityException, MalformattedElementException, NotPermittedOperationException {
        log.debug("Received request to purge a terminated Vertical Service instance");
        request.isValid();

        String tenantId = request.getTenantId();
        String vsiId = request.getVsiId();

        VerticalServiceInstance vsi = vsRecordService.getVsInstance(vsiId);
        if (tenantId.equals(adminTenant) || vsi.getTenantId().equals(tenantId)) {
            //TODO: at the moment the network slices are not purged, since assuming they are handled through another system
            vsRecordService.removeVsInstance(vsiId);
            removeVerticalServiceLcmManager(vsiId);
            log.debug("VSI purge action completed for VSI ID " + vsiId);
        } else {
            log.debug("Tenant " + tenantId + " is not allowed to purge VS instance " + vsiId);
            throw new NotPermittedOperationException("Tenant " + tenantId + " is not allowed to purge VS instance " + vsiId);
        }
    }

    @Override
    public void modifyVs(ModifyVsRequest request) throws NotExistingEntityException,
            MalformattedElementException, NotPermittedOperationException {
        log.debug("Method not implemented");
    }

    public void terminateVs(String vsiId, TerminateVsRequest request) throws NotExistingEntityException {
        log.debug("Processing VSI termination request for VSI ID " + vsiId);
        if (vsLcmManagers.containsKey(vsiId)) {
            String topic = "lifecycle.terminatevs." + vsiId;
            TerminateVsiRequestMessage internalMessage = new TerminateVsiRequestMessage(vsiId);
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                log.error("Error while translating internal VS termination message in Json format");
                vsRecordService.setVsFailureInfo(vsiId, "Error while translating internal VS termination message in Json format");
            }
        } else {
            log.error("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to terminate the VSI.");
            throw new NotExistingEntityException("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to terminate the VSI");
        }
    }

    public void removeVerticalServiceLcmManager(String vsiId) {
        log.debug("Vertical service " + vsiId + " has been terminated. Removing VS LCM from engine");
        this.vsLcmManagers.remove(vsiId);
        log.debug("VS LCM manager removed from engine");
    }

    public void notifyNetworkSliceStatusChange(VsmfNotificationMessage notification) {
        String networkSliceId = String.valueOf(notification.getNsiId());
        log.debug("Processing notification about status change for network slice " + networkSliceId);
        List<VerticalServiceInstance> vsis = vsRecordService.getAllVsInstances();
        String vsiId = null;
        for (VerticalServiceInstance vsi : vsis) {
            if (!vsi.getStatus().equals(VerticalServiceStatus.TERMINATED)) {
                if (vsi.getNetworkSliceId() != null && vsi.getNetworkSliceId().equals(networkSliceId)) {
                    vsiId = vsi.getVsiId();
                }
                log.debug("Network Slice " + networkSliceId + " is associated to vertical service " + vsiId);
                if (vsLcmManagers.containsKey(vsiId)) {
                    String topic = "lifecycle.notifyns." + vsiId;
                    NotifyNsiStatusChange internalMessage = new NotifyNsiStatusChange(networkSliceId, notification.getNsiStatus());
                    try {
                        sendMessageToQueue(internalMessage, topic);
                    } catch (Exception e) {
                        log.error("General exception while sending message to queue.");
                    }
                } else {
                    log.error("Unable to find Vertical Service LCM Manager for VSI ID " + vsiId + ". Unable to notify associated NS status change.");
                }
            }
        }
    }

    public void notifyNetworkSliceFailure(VsmfNotificationMessage notification) {
        //TODO: Currently the "FAILED" status is handled together with the status changes
        log.debug("Received network slice failure messages, but not able to process it at the moment. Skipped");
    }

    public void setNsmfLcmProvider(NsmfLcmProvisioningInterface nsmfLcmProvider) {
        this.nsmfLcmProvider = nsmfLcmProvider;
    }

    private VsBlueprint retrieveVsb(String vsbId) throws NotExistingEntityException, FailedOperationException {
        log.debug("Retrieving VSB with ID " + vsbId);
        try {
            QueryVsBlueprintResponse response = vsBlueprintCatalogueService.queryVsBlueprint(new GeneralizedQueryRequest(BlueprintCatalogueUtilities.buildVsBlueprintFilter(vsbId), null));
            return response.getVsBlueprintInfo().get(0).getVsBlueprint();
        } catch (MalformattedElementException e) {
            log.error("Malformatted request");
            return null;
        } catch (MethodNotImplementedException e) {
            log.error("Method not implemented.");
            return null;
        }
    }

    private void initNewVsLcmManager(String vsiId, String vsiName) {
        log.debug("Initializing new VS LCM manager for VSI ID " + vsiId);
        VsLcmManager vsLcmManager = new VsLcmManager(vsiId,
                vsiName,
                vsRecordService,
                vsDescriptorCatalogueService,
                vsBlueprintCatalogueService,
                translatorService,
                this,
                nsmfLcmProvider,
                vsmfUtils,
                translationRuleRepository);
        createQueue(vsiId, vsLcmManager);
        vsLcmManagers.put(vsiId, vsLcmManager);
        log.debug("VS LCM manager for VSI ID " + vsiId + " initialized and added to the engine");
    }

    private void sendMessageToQueue(VsmfEngineMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = Utilities.buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }

    private void createQueue(String vsiId, VsLcmManager vsiManager) {
        String queueName = ConfigurationParameters.engineQueueNamePrefix + vsiId;
        log.debug("Creating new Queue " + queueName + " in rabbit host " + rabbitHost);
        CachingConnectionFactory cf = new CachingConnectionFactory();
        cf.setAddresses(rabbitHost);
        cf.setConnectionTimeout(rabbitTimeout);
        RabbitAdmin rabbitAdmin = new RabbitAdmin(cf);
        Queue queue = new Queue(queueName, false, false, true);
        rabbitAdmin.declareQueue(queue);
        rabbitAdmin.declareExchange(messageExchange);
        rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(messageExchange).with("lifecycle.*." + vsiId));
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cf);
        MessageListenerAdapter adapter = new MessageListenerAdapter(vsiManager, "receiveMessage");
        container.setMessageListener(adapter);
        container.setQueueNames(queueName);
        container.start();
        log.debug("Queue created");
    }
}