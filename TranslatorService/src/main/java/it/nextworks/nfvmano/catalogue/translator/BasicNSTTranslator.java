/*
 * Copyright 2022 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogue.translator;

import it.nextworks.nfvmano.catalogue.blueprint.elements.VsDescriptor;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsdNstTranslationRule;
import it.nextworks.nfvmano.catalogue.blueprint.repo.TranslationRuleRepository;
import it.nextworks.nfvmano.catalogue.blueprint.repo.VsDescriptorRepository;
import it.nextworks.nfvmano.libs.vs.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MethodNotImplementedException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BasicNSTTranslator extends AbstractTranslator {

    private static final Logger log = LoggerFactory.getLogger(BasicNSTTranslator.class);

    private TranslationRuleRepository ruleRepo;

    public BasicNSTTranslator(VsDescriptorRepository vsdRepo, TranslationRuleRepository ruleRepo) {
        super(TranslatorType.BASIC_NST_TRANSLATOR, vsdRepo);
        this.ruleRepo = ruleRepo;
    }

    @Override
    public Map<String, NsInstantiationInfo> translateVsd(List<String> vsdIds)
            throws FailedOperationException, NotExistingEntityException, MethodNotImplementedException {
        log.debug("VSD->NST translation at basic NST translator");
        if (vsdIds.size() > 1)
            throw new FailedOperationException("Processing of multiple VSDs not supported by the basic MST translator");
        Map<String, NsInstantiationInfo> nfvNsInfo = new HashMap<>();
        Map<String, VsDescriptor> vsds = retrieveVsDescriptors(vsdIds);
        for (Map.Entry<String, VsDescriptor> entry : vsds.entrySet()) {
            String vsdId = entry.getKey();
            VsDescriptor vsd = entry.getValue();
            VsdNstTranslationRule rule = findMatchingTranslationRule(vsd);

            NsInstantiationInfo info = new NsInstantiationInfo(rule.getNstId(), null, null, null);
            nfvNsInfo.put(vsdId, info);
            log.debug("Added NS instantiation info for VSD " + vsdId + " - NST ID: " + rule.getNstId());
        }
        return nfvNsInfo;
    }

    private VsdNstTranslationRule findMatchingTranslationRule(VsDescriptor vsd) throws FailedOperationException, NotExistingEntityException {
        String vsbId = vsd.getVsBlueprintId();
        Map<String, String> vsdParameters = vsd.getQosParameters();
        return findMatchingTranslationRule(vsbId, vsdParameters);
    }

    private VsdNstTranslationRule findMatchingTranslationRule(String blueprintId, Map<String, String> descriptorParameters) throws FailedOperationException, NotExistingEntityException {
        if ((blueprintId == null) || (descriptorParameters.isEmpty()))
            throw new NotExistingEntityException("Impossible to translate descriptor into NST because of missing parameters");
        List<VsdNstTranslationRule> rules = ruleRepo.findByBlueprintId(blueprintId);
        for (VsdNstTranslationRule rule : rules) {
            if (rule.matchesVsdParameters(descriptorParameters)) {
                log.debug("Found translation rule");
                return rule;
            }
        }
        log.debug("Impossible to find a translation rule matching the given descriptor parameters");
        throw new FailedOperationException("Impossible to find a translation rule matching the given descriptor parameters");
    }

    private Map<String, VsDescriptor> retrieveVsDescriptors(List<String> vsdIds) throws NotExistingEntityException {
        log.debug("Retrieving VS descriptors from DB");
        Map<String, VsDescriptor> vsds = new HashMap<>();
        for (String vsdId : vsdIds) {
            Optional<VsDescriptor> vsd = vsdRepo.findByVsDescriptorId(vsdId);
            if (vsd.isPresent()) vsds.put(vsdId, vsd.get());
            else throw new NotExistingEntityException("Unable to find VSD with ID " + vsdId + " in DB");
        }
        return vsds;
    }
}
