package it.nextworks.nfvmano.catalogue.blueprint.messages;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.catalogue.blueprint.elements.VsdNstTranslationRule;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.vs.common.interfaces.InterfaceMessage;

import java.util.ArrayList;
import java.util.List;

public class OnBoardTranslationRuleRequest implements InterfaceMessage {

    List<VsdNstTranslationRule> vsdNstTranslationRuleList = new ArrayList<>();

    public OnBoardTranslationRuleRequest() {
    }

    /**
     * Constructor
     *
     * @param translationRules
     */
    public OnBoardTranslationRuleRequest(List<VsdNstTranslationRule> translationRules) {
        if (translationRules != null) vsdNstTranslationRuleList = translationRules;
    }

    public List<VsdNstTranslationRule> getVsdNsdTranslationRuleList() {
        return vsdNstTranslationRuleList;
    }

    public void setVsdNsdTranslationRuleList(List<VsdNstTranslationRule> vsdNstTranslationRuleList) {
        this.vsdNstTranslationRuleList = vsdNstTranslationRuleList;
    }

    /**
     * @return the translationRules
     */
    public List<VsdNstTranslationRule> getTranslationRules() {
        return vsdNstTranslationRuleList;
    }

    @JsonIgnore
    public void setBlueprintIdInTranslationRules(String blueprintId) {
        for (VsdNstTranslationRule tr : vsdNstTranslationRuleList)
            tr.setBlueprintId(blueprintId);
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (!vsdNstTranslationRuleList.isEmpty())
            for (VsdNstTranslationRule tr : vsdNstTranslationRuleList) tr.isValid();
    }
}