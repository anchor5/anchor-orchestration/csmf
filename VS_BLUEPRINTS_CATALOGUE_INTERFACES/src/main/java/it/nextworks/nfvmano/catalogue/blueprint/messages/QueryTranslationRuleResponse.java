package it.nextworks.nfvmano.catalogue.blueprint.messages;

import it.nextworks.nfvmano.catalogue.blueprint.elements.VsdNstTranslationRule;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.vs.common.interfaces.InterfaceMessage;

import java.util.ArrayList;
import java.util.List;

public class QueryTranslationRuleResponse implements InterfaceMessage {

    private List<VsdNstTranslationRule> vsdNstTranslationRules = new ArrayList<>();

    public QueryTranslationRuleResponse() {
    }

    public QueryTranslationRuleResponse(List<VsdNstTranslationRule> vsdNstTranslationRules) {
        if (vsdNstTranslationRules != null) this.vsdNstTranslationRules = vsdNstTranslationRules;
    }

    /**
     * @return the Translation Rules
     */
    public List<VsdNstTranslationRule> getVsdNsdTranslationRules() {
        return vsdNstTranslationRules;
    }

    @Override
    public void isValid() throws MalformattedElementException {
    }
}
