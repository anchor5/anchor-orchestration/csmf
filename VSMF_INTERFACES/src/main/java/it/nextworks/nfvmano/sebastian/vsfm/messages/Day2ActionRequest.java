package it.nextworks.nfvmano.sebastian.vsfm.messages;

import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.vs.common.interfaces.InterfaceMessage;

import java.util.Map;

public class Day2ActionRequest implements InterfaceMessage {

    private String vsiId;
    private String configRuleName;
    private Map<String, String> params;

    public Day2ActionRequest() {
    }

    public Day2ActionRequest(String vsiId, String configRuleName, Map<String, String> params) {
        this.vsiId = vsiId;
        this.configRuleName = configRuleName;
        this.params = params;
    }

    public String getVsiId() {
        return vsiId;
    }

    public void setVsiId(String vsiId) {
        this.vsiId = vsiId;
    }

    public String getConfigRuleName() {
        return configRuleName;
    }

    public void setConfigRuleName(String configRuleName) {
        this.configRuleName = configRuleName;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public void isValid() throws MalformattedElementException {
        if (vsiId == null) throw new MalformattedElementException("Modify VS request without VS instance ID");
        if (configRuleName == null) throw new MalformattedElementException("Modify VS request without new VSD ID");
    }
}

