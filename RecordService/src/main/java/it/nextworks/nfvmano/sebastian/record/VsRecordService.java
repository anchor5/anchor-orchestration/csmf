/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.sebastian.record;

import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotPermittedOperationException;
import it.nextworks.nfvmano.sebastian.record.elements.NetworkSliceStatus;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceInstance;
import it.nextworks.nfvmano.sebastian.record.elements.VerticalServiceStatus;
import it.nextworks.nfvmano.sebastian.record.repo.VerticalServiceInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Wrapper of the internal Vertical Slicer DB for the VSMF side,
 * with records about vertical services.
 *
 * @author nextworks
 */
@Service
public class VsRecordService {

    private static final Logger log = LoggerFactory.getLogger(VsRecordService.class);

    @Autowired
    private VerticalServiceInstanceRepository vsInstanceRepository;

    public synchronized String createVsInstance(String name, String description, String vsdId, String tenantId, Map<String, String> userData) {
        log.debug("Creating a new VS instance in DB.");
        VerticalServiceInstance vsi = new VerticalServiceInstance(null, vsdId, tenantId, name, description, null, userData);
        vsInstanceRepository.saveAndFlush(vsi);
        String vsiId = vsi.getId().toString();
        log.debug("Created Vertical Service instance with ID " + vsiId);
        vsi.setVsiId(vsiId);
        vsInstanceRepository.saveAndFlush(vsi);
        return vsiId;
    }

    public synchronized void removeVsInstance(String vsiId) throws NotExistingEntityException, NotPermittedOperationException {
        log.debug("Removing VS instance " + vsiId + " from DB.");
        VerticalServiceInstance vsi = getVsInstance(vsiId);
        if (!vsi.getStatus().equals(VerticalServiceStatus.TERMINATED))
            throw new NotPermittedOperationException("VS instance " + vsiId + " not in terminated status. Impossible to remove it from DB.");
        vsInstanceRepository.delete(vsi);
        log.debug("VS instance " + vsiId + " removed from DB.");
    }

    public synchronized void setVsFailureInfo(String vsiId, String errorMessage) {
        log.debug("Adding failure info to VS instance " + vsiId + " in DB.");
        try {
            VerticalServiceInstance vsi = getVsInstance(vsiId);
            vsi.setFailureState(errorMessage);
            vsInstanceRepository.saveAndFlush(vsi);
            log.debug("Set failure info in DB.");
        } catch (NotExistingEntityException e) {
            log.error("VSI with ID " + vsiId + " not present in DB. Impossible to set VSI failure info.");
        }
    }

    public synchronized void setVsStatus(String vsiId, VerticalServiceStatus status) throws NotExistingEntityException {
        log.debug("Setting status in VS instance " + vsiId + " in DB.");
        VerticalServiceInstance vsi = getVsInstance(vsiId);
        vsi.setStatus(status);
        vsInstanceRepository.saveAndFlush(vsi);
        log.debug("Set VS status in DB.");
    }

    public synchronized void setNsiInVsi(String vsiId, String nsiId) throws NotExistingEntityException {
        log.debug("Adding Network Slice instance " + nsiId + " to Vertical Service instance " + vsiId + " in VSI DB record.");
        VerticalServiceInstance vsi = getVsInstance(vsiId);
        vsi.setNetworkSliceId(nsiId);
        vsInstanceRepository.saveAndFlush(vsi);
        log.debug("VSI with ID " + vsiId + " updated with NSI " + nsiId);
    }

    public synchronized void setNsiStatusInVsi(String vsiId, String nsiId, NetworkSliceStatus networkSliceStatus) throws NotExistingEntityException {
        log.debug("Setting Network Slice instance " + nsiId + "status in Vertical Service instance " + vsiId + " in VSI DB record.");
        VerticalServiceInstance vsi = getVsInstance(vsiId);
        vsi.setNetworkSliceStatus(networkSliceStatus);
        vsInstanceRepository.saveAndFlush(vsi);
        log.debug("VSI with ID " + vsiId + " updated with NSI " + nsiId + " status " + networkSliceStatus);
    }

    public VerticalServiceInstance getVsInstance(String vsiId) throws NotExistingEntityException {
        log.debug("Retrieving VSI with ID " + vsiId + " from DB.");
        Optional<VerticalServiceInstance> vsi = vsInstanceRepository.findByVsiId(vsiId);
        if (vsi.isPresent()) return vsi.get();
        else throw new NotExistingEntityException("VSI with ID " + vsiId + " not present in DB.");
    }

    public List<VerticalServiceInstance> getAllVsInstances() {
        return vsInstanceRepository.findAll();
    }

    public List<VerticalServiceInstance> getAllVsInstances(String tenantId) {
        log.debug("Get vs instances for tenant:" + tenantId);
        return vsInstanceRepository.findByTenantId(tenantId);
    }
}
