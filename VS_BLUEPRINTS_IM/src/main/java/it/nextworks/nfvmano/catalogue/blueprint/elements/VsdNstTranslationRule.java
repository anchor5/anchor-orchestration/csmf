/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogue.blueprint.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.vs.common.exceptions.NotExistingEntityException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class VsdNstTranslationRule {

    @Id
    @GeneratedValue
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<VsdParameterValueRange> input = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String blueprintId;

    private String nstId;

    public VsdNstTranslationRule() {
    }

    public VsdNstTranslationRule(List<VsdParameterValueRange> input, String nstId) {
        if (input != null) this.input = input;
        this.nstId = nstId;
    }

    public Long getId() {
        return id;
    }

    public String getNstId() {
        return nstId;
    }

    /**
     * @return the input
     */
    public List<VsdParameterValueRange> getInput() {
        return input;
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
        return blueprintId;
    }


    /**
     * @param blueprintId the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
        this.blueprintId = blueprintId;
    }

    @JsonIgnore
    public boolean matchesNstId(String id) {
        if (nstId.equals(id)) return true;
        else return false;
    }

    @JsonIgnore
    public boolean matchesVsdParameters(Map<String, String> vsdParameters) {
        for (Map.Entry<String, String> entry : vsdParameters.entrySet()) {
            String parameterId = entry.getKey();
            try {
                VsdParameterValueRange vr = getVsdParameterValueRange(parameterId);
                String parameterValue = entry.getValue();
                if (!(vr.matchesVsdParameter(parameterValue))) return false;
            } catch (NotExistingEntityException e) {
                return false;
            }
        }
        return true;
    }

    @JsonIgnore
    private VsdParameterValueRange getVsdParameterValueRange(String parameterId) throws NotExistingEntityException {
        for (VsdParameterValueRange p : input) {
            if (p.getParameterId().equals(parameterId)) return p;
        }
        throw new NotExistingEntityException("VSD parameter not found in the rule");
    }

    public void isValid() throws MalformattedElementException {
        if ((input == null) || (input.isEmpty()))
            throw new MalformattedElementException("VSD NST translation rule without matching conditions");
        else for (VsdParameterValueRange vr : input) vr.isValid();
        if (nstId == null) throw new MalformattedElementException("VSD NST translation rule without NST Id");
    }
}

