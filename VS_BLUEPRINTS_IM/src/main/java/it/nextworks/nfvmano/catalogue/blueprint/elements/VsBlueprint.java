/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogue.blueprint.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity(name = "VsBlueprint")
@DiscriminatorValue("VSB")
public class VsBlueprint extends Blueprint {

    private SliceServiceType sliceServiceType;
    private EMBBServiceCategory embbServiceCategory;
    private URLLCServiceCategory urllcServiceCategory;

    public VsBlueprint() {
    }

    public VsBlueprint(String vsBlueprinId,
                       String version,
                       String name,
                       String description,
                       List<VsBlueprintParameter> parameters,
                       List<VsbEndpoint> endPoints,
                       List<String> configurableParameters,
                       List<ApplicationMetric> applicationMetrics,
                       SliceServiceType sliceServiceType,
                       EMBBServiceCategory embbServiceCategory,
                       URLLCServiceCategory urllcServiceCategory,
                       boolean interSite) {
        super(vsBlueprinId, version, name, description, parameters, endPoints,
                configurableParameters, applicationMetrics, interSite);
        if (sliceServiceType != null) this.sliceServiceType = sliceServiceType;
        if (embbServiceCategory != null) this.embbServiceCategory = embbServiceCategory;
        if (urllcServiceCategory != null) this.urllcServiceCategory = urllcServiceCategory;
    }

    public SliceServiceType getSliceServiceType() {
        return sliceServiceType;
    }

    public EMBBServiceCategory getEmbbServiceCategory() {
        return embbServiceCategory;
    }

    public URLLCServiceCategory getUrllcServiceCategory() {
        return urllcServiceCategory;
    }

    /**
     * @return the connection point towards the RAN segment
     */
    @JsonIgnore
    public String getRanEndPoint() {
        for (VsbEndpoint e : endPoints) {
            if (e.isRanConnection()) return e.getEndPointId();
        }
        return null;
    }

    public void isValid() throws MalformattedElementException {
        for (VsBlueprintParameter p : parameters) p.isValid();
        if (version == null) throw new MalformattedElementException("VS blueprint without version");
        if (name == null) throw new MalformattedElementException("VS blueprint without name");
        if (atomicComponents != null) {
            for (VsComponent c : atomicComponents) c.isValid();
        }
        if (serviceSequence != null) {
            for (VsbForwardingPathHop e : serviceSequence) e.isValid();
        }
        if (endPoints != null) {
            for (VsbEndpoint e : endPoints) e.isValid();
        }
        if (connectivityServices != null) {
            for (VsbLink l : connectivityServices) l.isValid();
        }

        if (sliceServiceType != null) {
            if (sliceServiceType.equals(SliceServiceType.EMBB)) {
                if (embbServiceCategory == null)
                    throw new MalformattedElementException("VSB without slice service category");

            } else if (sliceServiceType.equals(SliceServiceType.URLLC)) {
                if (urllcServiceCategory == null)
                    throw new MalformattedElementException("VSB without slice service category");
            }
        }
    }
}
