/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.nfvmano.catalogue.blueprint.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.nfvmano.libs.vs.common.exceptions.MalformattedElementException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class VsBlueprintInfo {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private String vsBlueprintId;
    private String vsBlueprintVersion;
    private String name;
    private String owner;

    @Transient
    private VsBlueprint vsBlueprint;

    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<String> activeVsdId = new ArrayList<>();

    public VsBlueprintInfo() {
    }


    /**
     * @param vsBlueprintId
     * @param vsBlueprintVersion
     * @param name
     * @param owner
     */
    public VsBlueprintInfo(String vsBlueprintId, String vsBlueprintVersion, String name, String owner) {
        this.vsBlueprintId = vsBlueprintId;
        this.vsBlueprintVersion = vsBlueprintVersion;
        this.name = name;
        this.owner = owner;
    }

    /**
     * @return the id
     */
    @JsonIgnore
    public Long getId() {
        return id;
    }

    /**
     * @return the vsBlueprint
     */
    public VsBlueprint getVsBlueprint() {
        return vsBlueprint;
    }

    /**
     * @param vsBlueprint the vsBlueprint to set
     */
    public void setVsBlueprint(VsBlueprint vsBlueprint) {
        this.vsBlueprint = vsBlueprint;
    }

    /**
     * @return the vsBlueprintId
     */
    public String getVsBlueprintId() {
        return vsBlueprintId;
    }

    /**
     * @return the vsBlueprintVersion
     */
    public String getVsBlueprintVersion() {
        return vsBlueprintVersion;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the activeVsdId
     */
    public List<String> getActiveVsdId() {
        return activeVsdId;
    }

    public void addVsd(String vsdId) {
        if (!(activeVsdId.contains(vsdId)))
            activeVsdId.add(vsdId);
    }

    @JsonIgnore
    public String getOwner() {
        return owner;
    }

    public void removeVsd(String vsdId) {
        if (activeVsdId.contains(vsdId))
            activeVsdId.remove(vsdId);
    }

    public void removeAllVsds() {
        this.activeVsdId = new ArrayList<String>();
    }

    public void isValid() throws MalformattedElementException {
        if (vsBlueprintId == null) throw new MalformattedElementException("VS Blueprint info without VS ID");
        if (vsBlueprintVersion == null) throw new MalformattedElementException("VS Blueprint info without VS version");
        if (name == null) throw new MalformattedElementException("VS Blueprint info without VS name");
    }
}
