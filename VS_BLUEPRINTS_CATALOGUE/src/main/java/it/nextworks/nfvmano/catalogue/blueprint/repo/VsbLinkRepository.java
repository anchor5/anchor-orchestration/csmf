package it.nextworks.nfvmano.catalogue.blueprint.repo;

import it.nextworks.nfvmano.catalogue.blueprint.elements.VsbLink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VsbLinkRepository extends JpaRepository<VsbLink, Long> {

}
