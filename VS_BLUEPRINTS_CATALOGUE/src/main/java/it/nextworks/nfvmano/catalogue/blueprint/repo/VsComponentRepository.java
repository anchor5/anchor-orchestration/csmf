package it.nextworks.nfvmano.catalogue.blueprint.repo;

import it.nextworks.nfvmano.catalogue.blueprint.elements.VsComponent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VsComponentRepository extends JpaRepository<VsComponent, Long> {

}
