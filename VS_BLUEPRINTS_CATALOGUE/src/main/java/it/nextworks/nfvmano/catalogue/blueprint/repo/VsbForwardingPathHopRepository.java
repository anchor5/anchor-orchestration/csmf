package it.nextworks.nfvmano.catalogue.blueprint.repo;

import it.nextworks.nfvmano.catalogue.blueprint.elements.VsbForwardingPathHop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VsbForwardingPathHopRepository extends JpaRepository<VsbForwardingPathHop, Long> {

}
