package it.nextworks.nfvmano.catalogue.blueprint.repo;

import it.nextworks.nfvmano.catalogue.blueprint.elements.ServiceConstraints;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceConstraintsRepository extends JpaRepository<ServiceConstraints, Long> {

}
